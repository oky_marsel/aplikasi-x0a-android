package marsel.oky.appx0a

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.menu_prodi.*
import kotlinx.android.synthetic.main.row_prodi.*
import org.json.JSONArray
import org.json.JSONObject

class mainProdi  : AppCompatActivity(), View.OnClickListener {

    lateinit var adapProdi: adapterProdi
    var daftarprodi = mutableListOf<HashMap<String,String>>()
    var url1 = "http://192.168.43.221/android/show_prodi.php"
    var url2 = "http://192.168.43.221/android/crud_prodi.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_prodi)
        adapProdi =  adapterProdi(daftarprodi,this)
        ls_prodi.layoutManager = LinearLayoutManager(this)
        ls_prodi.adapter = adapProdi
        bt_addprodi.setOnClickListener(this)
        bt_dltprodi.setOnClickListener(this)
        bt_updprodi.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        Showprodi()
    }

    fun Showprodi(){
        val request = StringRequest(Request.Method.POST,url1, Response.Listener { response ->
            daftarprodi.clear()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var prodi = HashMap<String,String>()
                prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
                prodi.put("id_prodi",jsonObject.getString("id_prodi"))
                daftarprodi.add(prodi)
            }
            adapProdi.notifyDataSetChanged()
        }, Response.ErrorListener { error ->
            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_addprodi -> {
                query("insert")
                daftarprodi.clear()
                Showprodi()

            }
            R.id.bt_updprodi -> {
                query("update")
                daftarprodi.clear()
                Showprodi()

            }
            R.id.bt_dltprodi -> {
                query("delete")
                daftarprodi.clear()
                Showprodi()
            }
        }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("id_prodi",ed_idprodi.text.toString())
                        hm.put("nama_prodi",ed_namaprodi.text.toString())

                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_prodi",ed_idprodi.text.toString())
                        hm.put("nama_prodi",ed_namaprodi.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_prodi",ed_idprodi.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}