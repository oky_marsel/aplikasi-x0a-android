package marsel.oky.appx0a

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.menu_prodi.*
import kotlinx.android.synthetic.main.row_prodi.*

class adapterProdi (val dataProdi : List<HashMap<String, String>>,
                    val mainProdi: mainProdi) : RecyclerView.Adapter<adapterProdi.HolderProdi>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): adapterProdi.HolderProdi {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_prodi,parent,false)
        return HolderProdi(v)
    }

    override fun getItemCount(): Int {
        return dataProdi.size
    }

    override fun onBindViewHolder(p0: adapterProdi.HolderProdi, p1: Int) {
        val data =dataProdi.get(p1)
        p0.idprodi.setText(data.get("id_prodi"))
        p0.namaprodi.setText(data.get("nama_prodi"))
        if (p1.rem(2)==0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))
        p0.cLayout.setOnClickListener(View.OnClickListener {
            mainProdi.ed_idprodi.setText(data.get("id_prodi"))
            mainProdi.ed_namaprodi.setText(data.get("nama_prodi"))
        })
    }

    class HolderProdi(v : View) :  RecyclerView.ViewHolder(v){
        val idprodi = v.findViewById<TextView>(R.id.tx_idprodi)
        val namaprodi = v.findViewById<TextView>(R.id.tx_namaprodi)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.rowProdi)
    }

}