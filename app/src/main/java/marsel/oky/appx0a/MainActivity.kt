package marsel.oky.appx0a

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.textclassifier.TextLinks
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.menu_prodi.*
import kotlinx.android.synthetic.main.row_mhs.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(),  View.OnClickListener {

    lateinit var mhsAdapter : AdapterDataMhs
    lateinit var mediaHelper: MediaHelper
    lateinit var prodiAdapter : ArrayAdapter<String>
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    var daftarProdi = mutableListOf<String>()
    val url = "http://192.168.43.221/android/show_data.php"
    val url2 = "http://192.168.43.221/android/get_nama_prodi.php"
    val url3 = "http://192.168.43.221/android/query_upd_del_ins.php"
    var imstr = ""
    var pilihProdi = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mhsAdapter = AdapterDataMhs(daftarMhs, this)
        mediaHelper =  MediaHelper(this)
        list_mhs.layoutManager = LinearLayoutManager(this)
        list_mhs.adapter = mhsAdapter
        im_upload.setOnClickListener(this)
        prodiAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line, daftarProdi)
        sp_prodi.adapter = prodiAdapter
        sp_prodi.onItemSelectedListener = itemselected
        bt_edit.setOnClickListener(this)
        bt_insert.setOnClickListener(this)
        bt_delete.setOnClickListener(this)
        bt_cari.setOnClickListener(this)
        //listMhs.addOnItemTouchListener(itemTouch)
    }

    override fun onStart() {
        super.onStart()
        showDataMhs()
        getnamaprodi()
    }
    val itemselected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sp_prodi.setSelection(0)
            pilihProdi = daftarProdi.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi = daftarProdi.get(position)
        }

    }
//    val itemTouch = object  : RecyclerView.OnItemTouchListener{
//        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
//        }
//        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
//            val view = p0.findChildViewUnder(p0.x,p1.y)
//            val tag = p0.getChildAdapterPosition(view!!)
//            val pos = daftarProdi.indexOf(daftarMhs.get(tag).get("nama_prodi"))
//
//            sp_prodi.setSelection(pos)
//            ed_nim.setText(daftarMhs.get(tag).get("nim").toString())
//            ed_namaMhs.setText(daftarMhs.get(tag).get("nama").toString())
//            Picasso.get().load(daftarMhs.get(tag).get("url")).into(im_upload)
//            return false
//        }
//
//        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
//        }
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imstr = mediaHelper.getBitmapToString(data!!.data,im_upload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object  : StringRequest(Method.POST,url3,
            Response.Listener {  response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                    showDataMhs()
                }else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT)
                    .show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+SimpleDateFormat("yyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nim",ed_nim.text.toString())
                        hm.put("nama",ed_namaMhs.text.toString())
                        hm.put("poto",imstr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)
                        hm.put("alamat",ed_alamat.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("nim",ed_nim.text.toString())
                        hm.put("nama",ed_namaMhs.text.toString())
                        hm.put("poto",imstr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)
                        hm.put("alamat",ed_alamat.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("nim",ed_nim.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getnamaprodi(){
        val request = StringRequest(Request.Method.POST,url2,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("nama_prodi"))
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMhs(){
        val request = StringRequest(Request.Method.POST,url,Response.Listener { response ->
                daftarMhs.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("nim",jsonObject.getString("nim"))
                    mhs.put("nama",jsonObject.getString("nama"))
                    mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    mhs.put("url",jsonObject.getString("url"))
                    mhs.put("alamat",jsonObject.getString("alamat"))
                    daftarMhs.add(mhs)
                }
                mhsAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
//        {
//            override fun getParams(): MutableMap<String, String> {
//                val hm = HashMap<String,String>()
//                hm .put("nama",namaMhs)
//                return  hm
//            }
//        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.im_upload -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.bt_insert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.bt_delete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.bt_edit ->{
                queryInsertUpdateDelete("update")
            }
//            R.id.bt_cari -> {
//                showDataMhs(ed_namaMhs.text.toString().trim())
//            }
        }
    }

}
