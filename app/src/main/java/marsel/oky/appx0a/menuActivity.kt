package marsel.oky.appx0a

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.menu_utama.*

class menuActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_utama)
        bt_mhs.setOnClickListener(this)
        bt_prodi.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.bt_mhs->{
                val menu =  Intent(this,MainActivity::class.java)
                startActivity(menu)
            }
            R.id.bt_prodi->{
                val menu = Intent(this,mainProdi::class.java)
                startActivity(menu)

            }
        }
    }

}